package com.bamboo.plugins.xml.report;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

public class XmlReportTestReportCollector implements TestReportCollector
{
    @NotNull
    @Override
    public TestCollectionResult collect(@NotNull File file) throws Exception
    {
        final XmlReportParser parser = new XmlReportParser();

        parser.setXpathTestSuites(this.xpathTestSuites);
        parser.setXpathTestSuiteName(this.xpathTestSuiteName);
        parser.setXpathTestSuiteTests(this.xpathTestSuiteTests);
        parser.setXpathTestSuiteTestName(this.xpathTestSuiteTestName);

        InputStream is = null;
        try
        {
            is = new FileInputStream(file);
            parser.parse(is);
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }

        return new TestCollectionResultBuilder()
                .addFailedTestResults(parser.getFailedTests())
                .addSuccessfulTestResults(parser.getPassedTests()).build();
    }

    private String xpathTestSuites;
    private String xpathTestSuiteName;
    private String xpathTestSuiteTests;
    private String xpathTestSuiteTestName;

    public void setXpathTestSuites(String xpath)
    {
        this.xpathTestSuites = xpath;
    }
    public void setXpathTestSuiteName(String xpath)
    {
        this.xpathTestSuiteName = xpath;
    }
    public void setXpathTestSuiteTests(String xpath)
    {
        this.xpathTestSuiteTests = xpath;
    }
    public void setXpathTestSuiteTestName(String xpath)
    {
        this.xpathTestSuiteTestName = xpath;
    }

    @NotNull
    @Override
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("xml");
    }
}
