package com.bamboo.plugins.xml.report;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class XmlReportTaskConfigurator extends AbstractTaskConfigurator implements TaskTestResultsSupport
{
    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("xpathTestSuites", params.getString("xpathTestSuites"));
        config.put("xpathTestSuiteName", params.getString("xpathTestSuiteName"));
        config.put("xpathTestSuiteTests", params.getString("xpathTestSuiteTests"));
        config.put("xpathTestSuiteTestName", params.getString("xpathTestSuiteTestName"));
        config.put("filePathPattern", params.getString("filePathPattern"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put("xpathTestSuites", "/OWASPZAPReport/site/alerts/alertitem");
        context.put("xpathTestSuiteName", "name");
        context.put("xpathTestSuiteTests", "instances/instance");
        context.put("xpathTestSuiteTestName", "uri");
        context.put("filePathPattern", "report.xml");
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("xpathTestSuites", taskDefinition.getConfiguration().get("xpathTestSuites"));
        context.put("xpathTestSuiteName", taskDefinition.getConfiguration().get("xpathTestSuiteName"));
        context.put("xpathTestSuiteTests", taskDefinition.getConfiguration().get("xpathTestSuiteTests"));
        context.put("xpathTestSuiteTestName", taskDefinition.getConfiguration().get("xpathTestSuiteTestName"));
        context.put("filePathPattern", taskDefinition.getConfiguration().get("filePathPattern"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);

        context.put("xpathTestSuites", taskDefinition.getConfiguration().get("xpathTestSuites"));
        context.put("xpathTestSuiteName", taskDefinition.getConfiguration().get("xpathTestSuiteName"));
        context.put("xpathTestSuiteTests", taskDefinition.getConfiguration().get("xpathTestSuiteTests"));
        context.put("xpathTestSuiteTestName", taskDefinition.getConfiguration().get("xpathTestSuiteTestName"));
        context.put("filePathPattern", taskDefinition.getConfiguration().get("filePathPattern"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String xpathTestSuitesValue = params.getString("xpathTestSuites");
        if (StringUtils.isEmpty(xpathTestSuitesValue))
        {
            errorCollection.addError("xpathTestSuites", getI18nBean().getText("com.check24.bamboo.plugins.xml.report.xpathTestSuites.error"));
        }

        final String xpathTestSuiteNameValue = params.getString("xpathTestSuiteName");
        if (StringUtils.isEmpty(xpathTestSuiteNameValue))
        {
            errorCollection.addError("xpathTestSuiteName", getI18nBean().getText("com.check24.bamboo.plugins.xml.report.xpathTestSuiteName.error"));
        }

        final String xpathTestSuiteTestsValue = params.getString("xpathTestSuiteTests");
        if (StringUtils.isEmpty(xpathTestSuiteTestsValue))
        {
            errorCollection.addError("xpathTestSuiteTests", getI18nBean().getText("com.check24.bamboo.plugins.xml.report.xpathTestSuiteTests.error"));
        }

        final String xpathTestSuiteTestNameValue = params.getString("xpathTestSuiteTestName");
        if (StringUtils.isEmpty(xpathTestSuiteTestNameValue))
        {
            errorCollection.addError("xpathTestSuiteTestName", getI18nBean().getText("com.check24.bamboo.plugins.xml.report.xpathTestSuiteTestName.error"));
        }

        final String filePathPatternValue = params.getString("filePathPattern");
        if (StringUtils.isEmpty(filePathPatternValue))
        {
            errorCollection.addError("filePathPattern", getI18nBean().getText("com.check24.bamboo.plugins.xml.report.filePathPattern.error"));
        }
    }
}
