package com.bamboo.plugins.xml.report;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.*;
import org.jetbrains.annotations.NotNull;

public class XmlReportTask implements TaskType
{
    private final TestCollationService testCollationService;

    public XmlReportTask(TestCollationService testCollationService)
    {
        this.testCollationService = testCollationService;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull final TaskContext taskContext)
    {
        final String pattern = taskContext.getConfigurationMap().get("filePathPattern");

        XmlReportTestReportCollector collector = new XmlReportTestReportCollector();
        collector.setXpathTestSuites(taskContext.getConfigurationMap().get("xpathTestSuites"));
        collector.setXpathTestSuiteName(taskContext.getConfigurationMap().get("xpathTestSuiteName"));
        collector.setXpathTestSuiteTests(taskContext.getConfigurationMap().get("xpathTestSuiteTests"));
        collector.setXpathTestSuiteTestName(taskContext.getConfigurationMap().get("xpathTestSuiteTestName"));

        testCollationService.collateTestResults(taskContext, pattern, collector);
        return TaskResultBuilder.create(taskContext).checkTestFailures().build();
    }
}