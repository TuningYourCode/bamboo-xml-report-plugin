package com.bamboo.plugins.xml.report;

import com.atlassian.bamboo.configuration.DefaultContentHandler;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Sets;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;
import java.util.Set;

public class XmlReportParser extends DefaultContentHandler
{
    private Set<TestResults> failedTests;
    private Set<TestResults> passedTests;

    public Set<TestResults> getFailedTests()
    {
        return failedTests;
    }

    public Set<TestResults> getPassedTests()
    {
        return passedTests;
    }

    private String xpathTestSuites;
    private String xpathTestSuiteName;
    private String xpathTestSuiteTests;
    private String xpathTestSuiteTestName;

    public void setXpathTestSuites(String xpath)
    {
        this.xpathTestSuites = xpath;
    }
    public void setXpathTestSuiteName(String xpath)
    {
        this.xpathTestSuiteName = xpath;
    }
    public void setXpathTestSuiteTests(String xpath)
    {
        this.xpathTestSuiteTests = xpath;
    }
    public void setXpathTestSuiteTestName(String xpath)
    {
        this.xpathTestSuiteTestName = xpath;
    }

    public void parse(InputStream inputStream)
    {
        failedTests = Sets.newLinkedHashSet();
        passedTests = Sets.newLinkedHashSet();

        try {

            XPath xpath = XPathFactory.newInstance()
                    .newXPath();
            InputSource xml = new InputSource(inputStream);
            NodeList testSuites = (NodeList) xpath.evaluate(this.xpathTestSuites, xml, XPathConstants.NODESET);

            for(int i = 0; i < testSuites.getLength(); i++)
            {
                Node testsuite = testSuites.item(i);

                NodeList testSuiteTests = (NodeList) xpath.evaluate(this.xpathTestSuiteTests, testsuite, XPathConstants.NODESET);

                String testSuiteName = (String) xpath.evaluate(this.xpathTestSuiteName, testsuite, XPathConstants.STRING);

                for(int j = 0; j < testSuiteTests.getLength(); j++)
                {
                    String testSuiteTestName = xpath.evaluate(this.xpathTestSuiteTestName, testSuiteTests.item(j));

                    TestResults test = new TestResults(testSuiteName, testSuiteTestName, new Long(0));
                    test.setState(TestState.FAILED);
                    test.addError(new TestCaseResultErrorImpl(testSuiteTestName));
                    failedTests.add(test);
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (failedTests.size() == 0) {
            TestResults test = new TestResults("Dummy", "Dummy", new Long(0));
            test.setState(TestState.SUCCESS);
            passedTests.add(test);
        }

    }

}
