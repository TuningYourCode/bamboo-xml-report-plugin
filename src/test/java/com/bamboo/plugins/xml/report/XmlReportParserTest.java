package com.bamboo.plugins.xml.report;

import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultError;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import junit.framework.TestCase;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class XmlReportParserTest extends TestCase
{
    public void testReportFile()
    {
        XmlReportParser parser = new XmlReportParser();

        InputStream is = getClass().getResourceAsStream("/test.xml");

        parser.setXpathTestSuites("/OWASPZAPReport/site/alerts/alertitem");
        parser.setXpathTestSuiteName("name");
        parser.setXpathTestSuiteTests("instances/instance");
        parser.setXpathTestSuiteTestName("uri");

        parser.parse(is);

        Iterator<TestResults> iter = parser.getFailedTests().iterator();
        assertEquals(2, parser.getFailedTests().size());
        assertEquals(0, parser.getPassedTests().size());

        TestResults failingTest = iter.next();
        assertEquals("Cookie No HttpOnly Flag", failingTest.getClassName());
        assertEquals("https://example.com/", failingTest.getActualMethodName());
        assertEquals(TestState.FAILED, failingTest.getState());
    }
}
