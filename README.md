##README##

This plugin will parse xml test results like OWASP ZAP report and build simple bamboo test results from that content.

You can will have to configure the xpath for the parts the plugin should use for the test results. Defaults configuration values will let you parse OWASP ZAP reports.

##Data security and privacy statement##

This plugin does not collect any data despite the task configuration. The task configuration will be saved in your bamboo database. The plugin will not download or upload any data to third party companies.
You can also check the source code to prove this statement and/or compile the plugin from source if you do not trust atlassian market place.
